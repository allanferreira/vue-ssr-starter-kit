FROM node:6-alpine
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY . /usr/src/app
EXPOSE 3000
RUN npm i
CMD npm start