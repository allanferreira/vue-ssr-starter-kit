# Vue SSR Starter Kit

> A Vue.js project with vue 2.0, vue-router and vuex starter kit for server side rendering. 

## Requirement

```bash
node 6.*
node 7.*
```

## Build

``` bash
npm i
npm run build
npm start
```

## Dev

```bash
npm i
npm run dev
```

## Reference resources

[https://ssr.vuejs.org/en](https://ssr.vuejs.org/en)

## License

[MIT](http://opensource.org/licenses/MIT)
