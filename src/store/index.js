import Vue from 'vue'
import Vuex from 'vuex'
import * as mutations from './mutations'
import * as actions from './actions'
import state from './state'

Vue.use(Vuex)

export default new Vuex.Store({
  state,
  actions,
  mutations
})
